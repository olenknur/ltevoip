package com.lte.ltevoip.api;

import android.content.Context;
import com.lte.ltevoip.helper.GlobalVars;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static Retrofit retrofit = null;

    private static Retrofit getClient(Context mContext) {
        if (retrofit == null) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS)
//                    .authenticator(new TokenAuthenticator(mContext))
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(GlobalVars.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }

    public static ApiInterface getApiService(Context ctx) {
        return getClient(ctx).create(ApiInterface.class);
    }
}
