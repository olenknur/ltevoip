package com.lte.ltevoip.api;


import com.lte.ltevoip.api.model.TokenResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

public interface ApiInterface {

    @GET("v1/members/zendesk/token")
    @Headers({
            "x-api-key: awjd9810u29h9u1j2iue1",
            "X-localization: en",
            "channel: web"
    })
    Call<TokenResponse> fetchToken(@Header("Authorization") String token);

}
