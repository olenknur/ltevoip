package com.lte.ltevoip.api.utils;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * An Util class to handle authentication.<br/>
 */

public final class AuthenticationUtils {

    /**
     * OAuth constants.
     */
    private static final String NONCE = "nonce";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String EXPIRES_IN = "expires_in";
    private static final String AUTH_PATH = "/auth/apps/api/access-token";
    private static final String ACCESS_TOKEN_GRANT_TYPE_HEADER = "grant_type";
    private static final String ACCESS_TOKEN_GRANT_TYPE = "client_credentials";
    private static final String LOG_OUT_PATH = "logout";

    /**
     * The authentication settings, include cloud service url, .
     */
    public static class Settings{
        private final String cloudUrl;
        private final String tenant;
        private final String userName;
        private String applicationId;
        private String secret;


        /**
         * Constructor.
         *
         * @param cloudUrl cloud server url
         * @param tenant tenant name
         * @param userName end user name
         * @param applicationId application id
         * @param secret application secret key given by cloud
         */
        public Settings(final String cloudUrl,
                        final String tenant,
                        final String userName,
                        final String applicationId,
                        final String secret) {
            this.cloudUrl = cloudUrl;
            this.tenant = tenant;
            this.userName = userName;
            this.applicationId = applicationId;
            this.secret = secret;
        }

        String getApplicationId() {
            return applicationId;
        }

        String getSecret() {
            return secret;
        }

        String getCloudUrl() {
            return cloudUrl;
        }
    }

    /**
     * The callback interface to notify the authentication result to caller.
     */
    public interface Callback{
        /**
         * If the authentication succeeds, this method will be invoked along with token
         * and expiration time of the token.
         * @param token access token returned from authentication service
         * @param expirationTime the expiration time of the given token
         */
        void onSuccess(String token, int expirationTime);

        /**
         * If the authentication application is rejected by authentication service, this method
         * will be invokde with response code.
         * @param responseCode the response code from authentication service
         */
        void onFailure(int responseCode);

        /**
         * If there is an exception raised during the authentication process, this method will be
         * invoked with the exception.
         * @param e The exception that breaks the authentication flow
         */
        void onException(Exception e);

        /**
         * If the authentication task is canceled, this method will be called.
         */
        void onCancellation();
    }

    /**
     * Perform authentication according to given settings.
     * @param settings authentication settings
     * @param callback the callback interface that application can be notified the authentication result
     *
     * @return AsyncTask to perform authentication
     */
    public static AsyncTask<Void, Void, String> authenticate(final Settings settings, final Callback callback) {

        final AuthenticationTask task = new AuthenticationTask(true, settings, callback);
        task.execute((Void) null);
        return task;
    }

    /**
     * Perform logout according to given settings.
     * @param settings authentication settings
     *
     * @return AsyncTask to perform authentication
     */
    public static AsyncTask<Void, Void, String> logout(final Settings settings) {

        final AuthenticationTask task = new AuthenticationTask(false, settings, null);
        task.execute((Void) null);
        return task;
    }

    /**
     * Represents an asynchronous authentication task used to authenticate the user.
     */
    private static class AuthenticationTask extends AsyncTask<Void, Void, String> {
        private final String TAG = AuthenticationTask.class.getCanonicalName();
        private final boolean isLogin;
        private final Settings settings;
        private final Callback callback;

        private String token;
        private int expiration;

        private int errorCode;

        private Exception exception;

        private AuthenticationTask(final boolean isLogin, final Settings settings, final Callback callback) {
            this.isLogin = isLogin;
            this.settings = settings;
            this.callback = callback;
        }

        @Override
        protected String doInBackground(Void... params) {
            if (settings.applicationId != null && !"".equals(settings.applicationId) &&
                    settings.secret != null && !"".equals(settings.secret)) {
                return getToken();
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (callback != null) {
                if (exception != null) {
                    callback.onException(exception);
                } else  if (token != null && expiration != -1) {
                    callback.onSuccess(token, expiration);
                } else {
                    callback.onFailure(errorCode);
                }
            }
        }

        @Override
        protected void onCancelled(String result) {
            if (callback != null) {
                callback.onCancellation();
            }
        }

        /**
         * Get Token directly.
         *
         */
        private String getToken() {
            try {
                Log.i(TAG, "Trying to get access token for tenant " + settings.tenant);

                attachSSLContext();

                final URL url;
                HttpURLConnection urlConnection;
                if (isLogin) {
                    url = new URL(buildOAuthURL());
                    urlConnection = getOAuthConnection(url);
                } else {
                    url = new URL(buildOAuthLogoutURL());
                    urlConnection = getOAuthLogOutConnection(url);
                }
                urlConnection.connect();

                if (isLogin) {
                    Log.i(TAG, "Get response code: " + urlConnection.getResponseCode());
                    if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        processSuccessfulAuth(urlConnection);
                    } else {
                        errorCode = urlConnection.getResponseCode();
                    }
                }

            } catch (Exception e) {
                exception = e;
            }

            return null;
        }

        /**
         * Build HttpURLConnection to OAuth.
         * @return HttpURLConnection connection
         */
        private HttpURLConnection getOAuthConnection(final URL url) throws IOException {

            Log.d("lfs","getOAuthConnection");
            Log.d("lfs","url"+url);
            final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            Log.d("lfs","getOAuthConnection2");

            urlConnection.setRequestProperty("Authorization", getBasicAuthToken());

            urlConnection.setRequestProperty ("Accept", "application/json");
            urlConnection.setRequestMethod("GET");
            return urlConnection;
        }

        private String getNonce() {
            return Long.toString(((Double) Math.floor((Math.random() * 1000000) + 1)).longValue());
        }

        /**
         * Generate the Basic-Auth header value.
         *
         * @return the header value.
         */
        private String getBasicAuthToken() {

            final String token = settings.getApplicationId() + ":" + settings.getSecret();

            return "Basic " + Base64.encodeToString(token.getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
        }

        private String buildOAuthLogoutURL() {
            StringBuilder buffer = new StringBuilder();
            buffer.append(settings.getCloudUrl());
            buffer.append(LOG_OUT_PATH);
            return buffer.toString();
        }

        private String buildOAuthURL() {

            StringBuilder buffer = new StringBuilder();
            buffer.append(settings.getCloudUrl());
            buffer.append(AUTH_PATH);

            buffer.append("?" + ACCESS_TOKEN_GRANT_TYPE_HEADER);
            buffer.append("=" + ACCESS_TOKEN_GRANT_TYPE);

            buffer.append("&" + NONCE + "=");
            buffer.append(getNonce());

            buffer.append("&name=");
            buffer.append(settings.userName);

            return buffer.toString();
        }

        /**
         * Build HttpURLConnection to logout from OAuth.
         * @return HttpURLConnection connection
         */
        private HttpURLConnection getOAuthLogOutConnection(final URL url) throws IOException {
            final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            final String authHeaderValue = "Bearer: " + token;
            urlConnection.setRequestProperty("Authorization", authHeaderValue);
            urlConnection.setRequestMethod("GET");
            return urlConnection;
        }

        /**
         * If it is https connection, attach the required SSLContext.
         *
         * @throws KeyManagementException if failed to initialize ssl context
         * @throws NoSuchAlgorithmException if https is not supported
         */
        private void attachSSLContext()
                throws KeyManagementException, NoSuchAlgorithmException {

            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, getTrustAllManager(), new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });

        }

        /**
         * Extract token and expiration time.
         * @param urlConnection the underlining http connection
         */
        private void processSuccessfulAuth(HttpURLConnection urlConnection) {
            try {
                BufferedReader bufferedReader = null;
                bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line+"\n");
                }
                bufferedReader.close();
                if (!parseToken(stringBuilder.toString())) {
                    throw new AuthenticationException("Failed to authenticate due to invalid response: " + stringBuilder.toString());
                }
            } catch (IOException e) {
                throw new AuthenticationException("Failed to authenticate due to IOException.");
            }
        }

        /**
         * Set configuration.
         * @param tokenString string retrieved from tenant
         *
         */
        private boolean parseToken(final String tokenString) {
            try {
                JSONObject tokenObject  = new JSONObject(tokenString);
                Log.e("Token", tokenObject.toString());
                token = tokenObject.getString(ACCESS_TOKEN);
                expiration = tokenObject.getInt(EXPIRES_IN);
                Log.e("Token", token);
            } catch (JSONException e) {
                Log.e(TAG, "Failed to parse behaviours");
                return false;
            }
            return true;
        }
    }

    /**
     * Test trust manager to accept all certificates.
     *
     * @return the trust all manager
     */
    private static TrustManager[] getTrustAllManager() {
        return new X509TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    @Override
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };
    }

}
